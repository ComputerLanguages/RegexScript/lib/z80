# Z80-Assembler
Assembler for the Z80 assembly language written in [RegexScript](https://github.com/PaulRaffer/RegexScript).

## Table of Contents

- [Installation](#installation)
- [Run](#run)
- [License](#license)


## Installation

### Prerequisites

Install [RegexScript](https://github.com/PaulRaffer/RegexScript) on your computer.

### Navigate to your project dircetory
```
cd <project-path>
```

**Example:**

```cd ~/projects/project1```


### Start RegexScript
```
regs core/all.regs z80/all.regs
```


### Run Assembler

```
>>> "<output-path>" << "run "<input-path>""
```

**Example:**


```>>> "./src/output.z80" << "run "./src/input.asm""```

## License

Distributed under the [Boost Software License, Version 1.0](https://www.boost.org/LICENSE_1_0.txt).
